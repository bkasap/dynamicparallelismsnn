import numpy as np
import pylab as plt


'''
Pulse coupled spiking neural network
Python code based on pulse coupled matlab code (Izhikevich, 2003)
'''

# number of excitatory and inhibitory neurons
Ne = 800
Ni = 200
# heterogenous network by randomly assigned neural parameters
re = np.random.rand(Ne)
ri = np.random.rand(Ni)
a = np.concatenate((0.02*np.ones(Ne), 0.02+0.08*ri))
b = np.concatenate((0.2*np.ones(Ne), 0.25-0.05*ri))
c = np.concatenate((-65+15*re**2, -65*np.ones(Ni)))
d = np.concatenate((8-6*re**2, 2*np.ones(Ni)))
# connectivity matrix, randomly assigned values modulated
# to generate a specific oscilatory activity
S = np.concatenate((0.5*np.random.rand(Ne+Ni, Ne), -np.random.rand(Ne+Ni, Ni)), axis=1)
# Initial values of state variables
v = -65*np.ones(Ne+Ni)
u = b*v
# Spike time and neuron id
firings = np.zeros(2)
# Initial values of synaptic currents
Isyn = np.zeros(Ne+Ni)

steps = 1000
for t in range(steps):
    I = np.concatenate((5*np.random.randn(Ne), 2*np.random.randn(Ni)))
    I = I+Isyn
    v = v+0.5*(0.04*v**2+5*v+140-u+I)
    v = v+0.5*(0.04*v**2+5*v+140-u+I)
    u = u+a*(b*v-u)
    fired = np.where(v>=30)[0]
    spikes = np.vstack((np.ones(len(fired))*t, fired)).T
    firings= np.vstack((firings, spikes))
    v[fired] = c[fired]
    u[fired] = u[fired]+d[fired]
    Isyn = np.sum(S[:,fired], axis=1)

plt.plot(firings[:,0], firings[:,1], '.')
plt.text(800, 1020, str(np.shape(firings)[0])+' spikes')
plt.xlabel("Time (ms)")
plt.ylabel("Neuron ID")
plt.show()
