#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <sstream>
#include <ctime>
#include <numeric>
#include <random>
#include <algorithm>

#include <curand.h>
#include <curand_kernel.h>
#include <cuda.h>
#include <cuda_profiler_api.h>

#include <sys/stat.h>

using namespace std;

class Neuron
{
	public:
		float	v;
		float 	u;
		float	a;
		float	b;
		float	c;
		float	d;
		float	I;
		int		nospks;
		int		neuronid;
};

// Membrane potential update
__device__ float dvdt(Neuron n){
	return (0.04f*n.v*n.v) + (5.0f*n.v) + 140 - n.u + n.I;
}

// Recovery varible update
__device__ float dudt(Neuron n){
	return n.a*((n.b*n.v)-n.u);
}

// Initialize the random states
__global__ void randgeneratorinit(unsigned int seed, curandState_t* states) {
	int id = threadIdx.x + blockIdx.x * blockDim.x;

	/* we have to initialize the state */
	curand_init(seed,	/* the seed can be the same for each core, here we pass the time in from the CPU */
			  id,		/* the sequence number should be different for each core (unless you want all
							 cores to get the same sequence of numbers for some reason - use thread id! */
			  0,		/* the offset is how much extra we advance in the sequence for each call, can be 0 */
			  &states[id]);
}

// Initialize the neural parameters
__global__ void initNeuron(int N_exc, int N, Neuron *neuron, curandState_t* state){
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	//printf("%d \n", id);

	curandState_t localState = state[id];
	float rand_float = curand_uniform(&localState);
	// initialize excitatory neuron parameters
	if ( id < N_exc ){
		neuron[id].a = 0.02f;
		neuron[id].b = 0.2f;
		neuron[id].c = -65.0f+(15.f*powf(rand_float,2.0f));
		neuron[id].d = 8.0f-(6.f*powf(rand_float,2.0f));
		neuron[id].v = -65.0f;
		neuron[id].u = neuron[id].v*neuron[id].b;
		neuron[id].I = 0.0f;
		neuron[id].nospks = 0;
		neuron[id].neuronid = id;
	}
	// initialize inhibitory neuron parameters
	else if (id >= N_exc and id < N ){
		neuron[id].a = 0.02f+(0.08f*rand_float);
		neuron[id].b = 0.25f-(0.05f*rand_float);
		neuron[id].c = -65.0f;
		neuron[id].d = 2.0f;
		neuron[id].v = -65.0f;
		neuron[id].u = neuron[id].v*neuron[id].b;
		neuron[id].I = 0.0f;
		neuron[id].nospks = 0;
		neuron[id].neuronid = id;
	}

	state[id] = localState;
}

// Propagate spikes with dynamic parallelization: AP-algorithm
__global__ void propagatespikes(int spiked, float *d_conn_w, int *d_conn_idx, float *d_Isyn, const int N, const int N_syn, Neuron *n){
	int id = threadIdx.x + blockIdx.x * blockDim.x;
	int dst_idx;
	//AP-Algorithm
	if ( id < N_syn and spiked < N ){
		dst_idx = (int)spiked*N_syn+id;
		atomicAdd(&d_Isyn[d_conn_idx[dst_idx]], d_conn_w[dst_idx]);
	}
}

// State update of neural variables
__global__ void stateupdate(Neuron *neuron,			// Neural parameters of individual neurons
							bool *spike,			// List of booleans to keep spiking neuron indices
							float *d_conn_w,		// Connectivity strengths
							int *d_conn_idx,		// Connection target neuron ids
							float *d_Isyn,			// Synaptic inputs
							curandState_t *state,	// States for random number generators (RNG)
							const int N,//			// Number of neurons
							const int N_exc,		// Number of excitatory neurons
							const int N_inh,		// Number of inhibitory neurons
							const int N_syn,		// Number of synapses per neuron
							int gridSize,			// Grid size and block size for the child kernels.
							int blockSize,
							int dynamic,
							int mode){
	int id = threadIdx.x + blockIdx.x * blockDim.x;

	if (!isfinite(neuron[id].I))
		neuron[id].I = 0.f;

	// Stochastic input current
	curandState_t localState = state[id];
	float rand_float = curand_normal(&localState);

	if ( id < N ){

		// Selecting the conductances for varying firing regimes
		float ge, gi;
		switch ( mode ){
		case 0: // quiet regime
			ge = 2.5f; gi = 1.0f;
			break;
		case 1: // balanced regime
			ge = 5.0f; gi = 2.0f;
			break;
		case 2: // irregular regime
			ge = 7.5f; gi = 3.0f;
			break;
		}

		if ( id < N_exc ){
			neuron[id].I = ge*rand_float;		//5.0 for balanced regime, 7.5 for irregular, 2.5 for quiet
		}
		else if (id >= N_exc and id < N ){
			neuron[id].I = gi*rand_float;		//2.0 for balanced regime, 3.0 for irregular, 1.0 for quiet
		}

		// Current each neuron receives at a timestep
		// sum of the stochastic and synaptic inputs
		neuron[id].I += d_Isyn[id];

		// update state variables
		neuron[id].v += 0.5f*(0.04f*neuron[id].v*neuron[id].v + 5.0f*neuron[id].v + 140.f - neuron[id].u + neuron[id].I);
		neuron[id].v += 0.5f*(0.04f*neuron[id].v*neuron[id].v + 5.0f*neuron[id].v + 140.f - neuron[id].u + neuron[id].I);
		neuron[id].u += neuron[id].a*(neuron[id].b*neuron[id].v-neuron[id].u);

		// initialize currents for the next step already
		d_Isyn[id] = 0.f;

		// check if any neuron's membrane potential passed the spiking threshold
		if ( neuron[id].v >= 30.f or !isfinite(neuron[id].v) ){
			spike[id] = true;
			neuron[id].v = neuron[id].c;
			neuron[id].u += neuron[id].d;
			neuron[id].nospks ++;
			// AP-algorithm for spike propagation
			// dynamic: 0 AP-algorithm
			if ( dynamic == 0 )
				propagatespikes<<<gridSize, blockSize>>>(id, d_conn_w, d_conn_idx, d_Isyn, N, N_syn, neuron);

		}
		else{
			spike[id] = false;
		}
	}

	state[id] = localState;
}

// N-algorithm
__global__ void deliverspks1(Neuron *neuron,		// Neural parameters of individual neurons
							bool *spike,			// List of booleans to keep spiking neuron indices
							float *d_conn_w,		// Connectivity strengths
							int *d_conn_idx,		// Connection target neuron ids
							float *d_Isyn,			// Synaptic inputs
							const int N,			// Number of neurons
							const int N_exc,		// Number of excitatory neurons
							const int N_inh,		// Number of inhibitory neurons
							const int N_syn){		// Number of synapses per neuron
	int id = threadIdx.x + blockIdx.x * blockDim.x;

	// N-algorithm
	// a thread for each neuron (presynaptic)
	if (id < N){
		// for each presynaptic neuron
		for (int dst_idx=0; dst_idx<N_syn; dst_idx++){
			// check if there is a spike from presynaptic neurons
			//d_conn_idx[id*N_syn+dst_idx];
			if (spike[id] == true){
				atomicAdd(&d_Isyn[d_conn_idx[id*N_syn+dst_idx]], d_conn_w[id*N_syn+dst_idx]);
			}
		}
	}
}

// S-algorithm
__global__ void deliverspks2(Neuron *neuron,		// Neural parameters of individual neurons
							bool *spike,			// List of booleans to keep spiking neuron indices
							float *d_conn_w,		// Connectivity strengths
							int *d_conn_idx,		// Connection target neuron ids
							float *d_Isyn,			// Synaptic inputs
							const int N,			// Number of neurons
							const int N_exc,		// Number of excitatory neurons
							const int N_inh,		// Number of inhibitory neurons
							const int N_syn){		// Number of synapses per neuron
	int id = threadIdx.x + blockIdx.x * blockDim.x;

	int src;
	// S-algorithm
	// a thread for each synapse
	if (id < N*N_syn){
		src = (int) (id/N_syn);
		// check if there is a spike from presynaptic neurons
		if (spike[src] == true){
			atomicAdd(&d_Isyn[d_conn_idx[id]], d_conn_w[id]);
		}
	}
}

// Write filename
// helper functions to write simulation results in files
const char * filename(char * buffer, string varname){
	string fname;
	fname = buffer + varname;
	return fname.c_str();
}

void writeNeuronInfo(std::ofstream& output, Neuron n){
	output << n.a << " " << n.b << " " << n.c << " " << n.d << " " << n.v << " " << n.u << " " << n.I << " " << n.neuronid << " " << n.nospks << "\n";
}

// main function simulates the same network (N and S)
// for different firing regimes (mode: 0 quiet, 1 balanced, 2 irregular)
// with all the synaptic update algorithms (dynamic: 0 AP-algorithm, 1 N-algorithm, 2 S-algorithm)

int main(int argc, char **argv){
	// Number of neurons in the network: N
	// Number of synapses per neuron: N_syn
	const int N = atoi(argv[1]);
	const int N_syn = atoi(argv[2]);

	// Number of excitatory 80% and inhibitory 20% connections
	const int N_exc = ceil(4*N/5);
	const int N_inh = ceil(N/5);

	cout << "Number of neurons: " << N << "\nnumber of synapses per neuron: "  << N_syn << "\n";
	printf("%d Neurons in the network: %d excitatory and %d inhibitory \n", N, N_exc, N_inh);

	printf("Allocating space on GPU memory: \n");
	// Allocate space on GPU memory for neural parameters
	// for N neurons at time-steps t_n and t_(n+1)
	Neuron *d_Neuron, *h_Neuron;
	h_Neuron = (Neuron *)malloc(N*sizeof(Neuron));
	cudaGetErrorString(cudaMalloc(&d_Neuron, N*sizeof(Neuron)));

	// for N neurons to keep spikes
	bool *d_spikes, *h_spikes;
	h_spikes = (bool *)malloc(N*sizeof(bool));
	cudaGetErrorString(cudaMalloc(&d_spikes, N*sizeof(bool)));
	printf("Memory allocated for neurons\n");

	// for connectivity matrix
	float *d_conn_w; float *h_conn_w;
	h_conn_w = (float *)malloc(N_syn*N*sizeof(float));
	cudaGetErrorString(cudaMalloc(&d_conn_w, N_syn*N*sizeof(float)));
	printf("Memory allocated for connectivity matrix\n");

	// allocate memory on the GPU memory for the connectivity
	int *d_conn_idx, *h_conn_idx;
	h_conn_idx = (int *)malloc(N_syn*N*sizeof(int));
	cudaGetErrorString(cudaMalloc(&d_conn_idx, N_syn*N*sizeof(int)));

	// for synaptic input to N neurons
	float *d_Isyn;
	cudaGetErrorString(cudaMalloc(&d_Isyn, N*sizeof(float)));
	printf("Memory allocated for synapses\n");

	// gridSize and blockSize for N operations
	int blockSizeN;		// The launch configurator returned block size
	int minGridSize;	// The minimum grid size needed to achieve the
						// maximum occupancy for a full device launch
	int gridSizeN;		// The actual grid size needed, based on input size
	cudaOccupancyMaxPotentialBlockSize(&minGridSize, &blockSizeN, initNeuron, 0, 0);
	// Round up according to array size
	gridSizeN = (N + blockSizeN - 1) / blockSizeN;

	// initialize random number generator to be used for stochastic input
	printf("Initializing random number generators\n");
	curandState_t *devStates;
	cudaGetErrorString(cudaMalloc((void **)&devStates, N*sizeof(curandState_t)));
	randgeneratorinit<<<gridSizeN, blockSizeN>>>(time(NULL), devStates);
	cudaDeviceSynchronize();

	// Initialize connectivity matrix on the GPU
	printf("Initializing random connectivity matrix values\n");

	std::mt19937 rng(time(NULL));
	{	// fill h_conn_idx using https://ieeexplore.ieee.org/document/9206752
		std::exponential_distribution<float> exp;
		auto row = reinterpret_cast<float*>(h_conn_idx);

		for(int i=0; i < N; i++){ // run over neurons
			// prefix sum of exp. distr. rnd. no.
			std::generate(row, row + N_syn, [&, x = 0.0f]() mutable { x += std::min(10.0f, exp(rng)); return x; });
			// scale to [0, N) without dups and convert back to int
			const float scale = (N-N_syn) / (row[N_syn-1] + std::min(10.0f, exp(rng)));
			std::transform(row, row + N_syn, h_conn_idx + i*N_syn, [scale, idx=0](float x) mutable {
				return static_cast<int>(std::roundf(x * scale)) + (idx++);
			});
			row += N_syn;
		}
	}

	// fill h_conn_w
	{	// excitatory neurons
		std::uniform_real_distribution<float> iid(0.0f, (1000.0f/N_syn) * 0.5f);
		std::generate(h_conn_w, h_conn_w + N_exc * N_syn, [&]{ return iid(rng); });
	}
	{	// inhibitory neurons
		std::uniform_real_distribution<float> iid(0.0f, (1000.0f/N_syn) * -1.0f);
		std::generate(h_conn_w + N_exc * N_syn, h_conn_w + N * N_syn, [&]{ return iid(rng); });
	}

	// Copy connectivity matrix to GPU memory
	printf("Retrieving initial connectivity matrix\n");
	cudaGetErrorString(cudaMemcpy(d_conn_w, h_conn_w, N_syn*N*sizeof(float), cudaMemcpyHostToDevice));
	cudaGetErrorString(cudaMemcpy(d_conn_idx, h_conn_idx, N_syn*N*sizeof(int), cudaMemcpyHostToDevice));
	cudaDeviceSynchronize();

	// CUDA kernel initiation parameters
	// gridSize and blockSize for N operations
	int blockSizeNNsyn;		// The launch configurator returned block size
	int minGridSizeNNsyn;	// The minimum grid size needed to achieve the
							// maximum occupancy for a full device launch
	int gridSizeNNsyn;		// The actual grid size needed, based on input size
	cudaOccupancyMaxPotentialBlockSize(&minGridSizeNNsyn, &blockSizeNNsyn, deliverspks2, 0, 0);

	// Round up according to array size
	gridSizeNNsyn = (N*N_syn + blockSizeNNsyn - 1) / blockSizeNNsyn;

	// run this network configuration
	// for three firing states (mode: 0 quiet, 1 balanced, 2 irregular)
	// with three algorithms (dynamic: 0 AP, 1 N, 2 S)
	for (int mode=0; mode<3; mode++){
		for (int dynamic=0; dynamic<3; dynamic++){

			// Initialize neural parameters for excitatory and inhibitory neurons
			printf("Initializing neuron parameters\n");
			initNeuron<<<gridSizeN, blockSizeN>>>(N_exc, N, d_Neuron, devStates);
			cudaDeviceSynchronize();

			// Copy initial values of neural parameters back to CPU
			printf("Retrieving initial parameter values\n");
			cudaGetErrorString(cudaMemcpy(h_Neuron, d_Neuron, N*sizeof(Neuron), cudaMemcpyDeviceToHost));
			cudaDeviceSynchronize();

			// Print out the simulation protocol on the screen
			cout << "===================================================== \n";
			cout << "Neurons: " << N << " Synapses: " << N_syn << " Dynamic (Algorithm): " << dynamic << " Mode (State): " << mode << "\n";
			cout << "===================================================== \n";

			// Open file streams to write data
			// Make folder to save simulation results into
			char buffer[100];
			snprintf(buffer, sizeof(buffer), "Results/");
			mkdir(buffer, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
			snprintf(buffer, sizeof(buffer), "Results/N%dNsyn%dRegime%dAlg%d/", N, N_syn, mode, dynamic);

			mkdir(buffer, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

			// Write initial neural values in the file neuroninfo.csv
			string fname;
			printf("Initializing file streams to write neuron info\n");
			fname = filename(buffer, "neuroninfo.csv");
			ofstream neuroninfo(fname.c_str());

			neuroninfo << "a b c d v u I id nospks" <<  "\n";
			for ( int i=0; i<N; i++ ){
				writeNeuronInfo(neuroninfo, h_Neuron[i]);
			}

			// File to write spike times and spiking neuron ids
			fname = filename(buffer, "spiketimes.csv");
			ofstream spikes(fname.c_str());
			spikes << "SpkTime NeuronID" << "\n";

			// File to write compute time per timestep
			fname = filename(buffer, "computetime.csv");
			ofstream computetime(fname.c_str());
			computetime << "TimeStep SpksPerStep TimeKernelUpdate TimeSpent" << "\n";

			// initialize clocks for timekeeping
			clock_t start_sim, end_sim, start, middle, end;
			int spkcount;
			double took, took1, elapsed;
			elapsed = 0;

			cout << "\nStarting simulation and timer...\n";
			start_sim = clock();
			int totalspkcount = 0;

			cudaProfilerStart();

			// Start main simulation loop
			for (int tstep=0; tstep<2000; tstep++){

				spkcount = 0;
				cudaDeviceSynchronize();
				start = clock();
				// State update
				stateupdate<<<gridSizeN, blockSizeN>>>(d_Neuron, d_spikes, d_conn_w, d_conn_idx, d_Isyn, devStates, N, N_exc, N_inh, N_syn, gridSizeN, blockSizeN, dynamic, mode);

				middle = clock();
				switch ( dynamic ){
				case 0:
					// if dynamic parallelization, stateupdate kernel already calculated
					break;
				case 1:
					// parallelization over neurons: N-algorithm
					cudaDeviceSynchronize();
					deliverspks1<<<gridSizeN, blockSizeN>>> (d_Neuron, d_spikes, d_conn_w, d_conn_idx, d_Isyn, N, N_exc, N_inh, N_syn);
					break;
				case 2:
					// parallelization over synapses: S-algorithm
					cudaDeviceSynchronize();
					deliverspks2<<<gridSizeNNsyn, blockSizeNNsyn>>> (d_Neuron, d_spikes, d_conn_w, d_conn_idx, d_Isyn, N, N_exc, N_inh, N_syn);
					break;
				}

				cudaDeviceSynchronize();
				end = clock();

				cudaGetErrorString(cudaMemcpy(h_Neuron, d_Neuron, N*sizeof(Neuron), cudaMemcpyDeviceToHost));
				cudaGetErrorString(cudaMemcpy(h_spikes, d_spikes, N*sizeof(bool), cudaMemcpyDeviceToHost));
				//cudaDeviceSynchronize();

				for ( int i=0; i<N; i++){
					if ( h_spikes[i] == true ){
						spikes << tstep << " " << i << "\n";
						spkcount++;
					}
				}

				took1 = double(end-middle) / CLOCKS_PER_SEC * 1000;
				took = double(end-start) / CLOCKS_PER_SEC * 1000;
				elapsed += took;

				computetime << tstep << " " << spkcount << " " << took1 << " " << took << "\n";

				totalspkcount += spkcount;

			}
			end_sim = clock();
			cudaProfilerStop();
			cout << "\nClocks per sec is " << CLOCKS_PER_SEC << "\n";
			cout << "\nEnd of simulation...\n";
			cout << "Simulation took: " << double(end_sim-start_sim) / CLOCKS_PER_SEC * 1000<< " ms.\n";
			cout << "Without data transfers: " << elapsed << " ms.\n";
			cout << "Total number of spikes: " << totalspkcount << "\n\n\n";

			// Write simulation overview in the file sim_overview.csv
			ofstream sim_overview(filename(buffer, "sim_overview.csv"));
			sim_overview << "N elapsed(ms) elapsedwithdata(ms) totalspkcount" <<  "\n";
			sim_overview << N << " " << elapsed << " " << double(end_sim-start_sim) / CLOCKS_PER_SEC * 1000 << " " << totalspkcount;


			// Write neural values in the file neuroninfo.csv
			ofstream neuroninfoafter(filename(buffer, "neuroninfoafter.csv"));
			neuroninfoafter << "a b c d v u I id nospks" <<  "\n";
			for ( int i=0; i<N; i++ ){
				writeNeuronInfo(neuroninfoafter, h_Neuron[i]);
			}

		}
	}
	cudaDeviceReset();
}
